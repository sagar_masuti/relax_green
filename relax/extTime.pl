#!/usr/bin/perl

if ($#ARGV != 0) 
{
	print "usage: extractTime filename\n";
	exit;
}

my $ConStr = "Time taken to execute" ;
my %timings ;
my %count ;
my $bPresent = 0 ;
my $temp = 0 ;

#open a file
open (MYFILE, $ARGV[0]);
while (<MYFILE>) {

	$temp = index ($_, 'execute') ;
	if ($temp != -1)
	{
		my $str = substr $_, length($ConStr), (length ($_) - length ($ConStr)) ;

		my @val = split(':', $str) ;	

		foreach my $k (keys %timings)
		{
			if ($k eq @val[0])
			{
				$bPresent = 1 ;
				$count{$k} = $count{$k} + 1 ;
				$timings{$k} = $timings{$k} + @val[1] ;
			}
  		}
		if ($bPresent == 0)
		{
			$timings{@val[0]} = @val[1] ;	
			$count{@val[0]} = 1 ;
		}
		$bPresent = 0 ;
	} 
 }

 my $avg ;
 my $sub ;

 foreach my $k (sort keys %timings)
 {
	$k =~ s/^\s+|\s+$//g ;	
 }
 print "\n" ;
 foreach my $k (sort keys %timings)
 {
        $avg = $timings{$k}/$count{$k} ;
        $sub = sprintf('%.5f', $avg);
        print "$k = $sub," ;
 		print "\n" ;
 }
 print "\n" ;


 close (MYFILE); 

