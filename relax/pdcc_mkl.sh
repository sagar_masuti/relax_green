
#module load cuda60/toolkit/6.0.37 
export CC=icc
export FC=ifort
module load intel/compiler/64/13.1/2013.5.192 
module load intel/mkl/64/11.0/2013.5.192  
./waf


if ! [ $? = 0 ]; then
	rm -rf build
	#./waf configure --use-fftw --fftw-dir=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3 --use-cuda --cuda-dir=/cm/shared/apps/cuda50/toolkit/current/ --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0
	./waf configure --use-papi --papi-dir=/cm/shared/apps/papi-5.2.0 --mkl-dir=/cm/shared/apps/intel/composer_xe/2013.5.192/mkl/lib/intel64 --gmt-dir=/cm/shared/apps/gmt/4.5.12 
	./waf
fi 

export PATH=$PATH:$PWD/build

sbatch ./examples/tutorial/run2.sh
