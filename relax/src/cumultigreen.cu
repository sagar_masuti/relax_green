/*-----------------------------------------------------------------------
! Copyright 2013 Sylvain Barbot
!
! This file is part of RELAX
!
! RELAX is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! RELAX is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with RELAX.  If not, see <http://www.gnu.org/licenses/>.
!
! \author Sagar Masuti 
!----------------------------------------------------------------------*/

#include <stdio.h>
#include "cuinclude.h"
#include <cufft.h>

#if defined(USING_CUDA) && defined(MULTI_GPU)


/* ----------------------------- Forward declaration ---------------------------------- */

/* --------------------------------- Device Functions --------------------------------- */
/* --------------------------------- Device Functions --------------------------------- */

/* --------------------------------- Global Functions --------------------------------- */
/* --------------------------------- Global Functions --------------------------------- */

/* --------------------------------- Host Functions ----------------------------------- */
void cuMultiAlloc (ST_MULTI_GPU_CTX *pstCtx) ;

void cuMultiCopyData (ST_MULTI_GPU_CTX *pstCtx, float *fData1, float *fData2,
                      float *fData3, E_MULTI_DIRECTION eDir) ;

void cuMultiHandleError(ST_MULTI_GPU_CTX *pstCtx) ;
/* --------------------------------- Host Functions ----------------------------------- */

/* --------------------------------- util functions ----------------------------------- */
/* --------------------------------- util functions ----------------------------------- */


ST_MULTI_GPU_CTX* 
cuMultiInit(int iSx1, int iSx2, int iSx3)
{
    ST_MULTI_GPU_CTX *pstCtx = NULL ;

    // Need to check if the problem fits on the GPU's
    pstCtx = (ST_MULTI_GPU_CTX *) calloc(1, sizeof(ST_MULTI_GPU_CTX)) ;
    if (NULL != pstCtx)
    {
        pstCtx->iOrigSx1 = iSx1 ;
        pstCtx->iOrigSx2 = iSx2 ;
        pstCtx->iOrigSx3 = iSx3 ;
        pstCtx->iMultiSx1 = iSx1 / MAX_NUM_GPU ;
        pstCtx->iMultiSx2 = iSx2 / MAX_NUM_GPU ;
        pstCtx->iMultiSx3 = iSx3 / MAX_NUM_GPU ;
        pstCtx->iSize = (sizeof (float) * (pstCtx->iMultiSx1+2) *
                        (pstCtx->iMultiSx2) * (pstCtx->iMultiSx3)) ; 
    
		printf ("Address before : %p", pstCtx->fC1[0]) ;   
	    cuMultiAlloc (pstCtx) ; 
		printf ("Address after : %p", pstCtx->fC1[0]) ;   
	}

    return pstCtx ; 
}

void
cuMultiElasticResponse(ST_MULTI_GPU_CTX *pstCtx, float *fData1, float *fData2,
                       float *fData3, double dDx1, double dDx2, double dDx3, 
                       double dRatio1, double dRatio2) 
{
    int 			iIndex ;
    dim3 			dimGrid(1, 1, 1) ;
    dim3 			dimBlock(1, 1, 1) ;
	cudaError_t 	cuError = cudaSuccess ;
	int 			iSx1 ;

#ifdef PRINT_ENTRY_EXIT
	printf ("Entered cuMultiElasticResponse\n") ;
#endif

    dimGrid.x=pstCtx->iMultiSx3 ;
    dimGrid.y=pstCtx->iMultiSx2 ;
    
    cuMultiCopyData (pstCtx, fData1, fData2, fData3, E_MULTI_DEVICE_TO_DEVICE_FORWARD) ;

    for (iIndex = 0; iIndex < MAX_NUM_GPU; iIndex++)
    {
        cuError = cudaSetDevice (iIndex) ;
        if (cudaSuccess != cuError)
        {
            printf ("cuMultiElasticResponse : cudaSetDevice Failed\n") ;
            return ;
        } 
        if (iIndex == MAX_NUM_GPU-1)
		{
			iSx1 = (pstCtx->iMultiSx1 + 2)/MAX_NUM_GPU;
    		dimBlock.x = iSx1 ;
		}
		else
		{
			iSx1 = pstCtx->iMultiSx1/MAX_NUM_GPU;
    		dimBlock.x = iSx1 ;
		}
        cuElasticResKernel <<<dimGrid, dimBlock>>> 
                           (pstCtx->fC1[iIndex], pstCtx->fC2[iIndex], pstCtx->fC3[iIndex],
                           iSx1, pstCtx->iMultiSx2, pstCtx->iMultiSx3, dDx1, dDx2, dDx3, 
						   dRatio1, dRatio2, pstCtx->iOrigSx1, pstCtx->iOrigSx2,
                           pstCtx->iOrigSx3, iIndex) ; 

		cuError = cudaGetLastError () ;
    	if (cudaSuccess != cuError)
        {
            printf ("cuMultiElasticResponse : Kernel call Failed\n") ;
            cuMultiHandleError(pstCtx) ;
        	return ;
		}
	}
   
    cuMultiCopyData (pstCtx, fData1, fData2, fData3, E_MULTI_DEVICE_TO_DEVICE_BACKWARD) ; 

#ifdef PRINT_ENTRY_EXIT
	printf ("Exited cuMultiElasticResponse\n") ;
#endif
}

void cuMultiAlloc (ST_MULTI_GPU_CTX *pstCtx)
{
    int iIndex ; 
    cudaError_t cuError = cudaSuccess ;

    for (iIndex = 0; iIndex < MAX_NUM_GPU; iIndex++)
    {
        cuError = cudaSetDevice (iIndex) ;
        if (cudaSuccess != cuError)
        {
            printf ("cuMultiAlloc : cudaSetDevice Failed\n") ;
            return ;
        } 
        cuError = cudaMalloc((void**)&(pstCtx->fC1[iIndex]), pstCtx->iSize) ;
        if (cudaSuccess != cuError)
        {
            printf ("cuMultiAlloc : Failed to allocate memory for fC1 on: %d\n", iIndex) ;
            cuMultiHandleError(pstCtx) ;
        }
        cuError = cudaMalloc((void**)&(pstCtx->fC2[iIndex]), pstCtx->iSize) ;
        if (cudaSuccess != cuError)
        {
            printf ("cuMultiAlloc : Failed to allocate memory for fC2 on: %d\n", iIndex) ;
            cuMultiHandleError(pstCtx) ;
        }
        cuError = cudaMalloc((void**)&(pstCtx->fC3[iIndex]), pstCtx->iSize) ;
        if (cudaSuccess != cuError)
        {
            printf ("cuMultiAlloc : Failed to allocate memory for fC3 on: %d\n", iIndex) ;
            cuMultiHandleError(pstCtx) ;
        }
    }
}


void cuMulitCopy (ST_MULTI_GPU_CTX *pstCtx, float *pfSrc, E_MULTI_COPY_TYPE eType,
                  E_MULTI_DIRECTION eDirection)
{
    int 			iIndex ;
    cudaError_t 	cuError = cudaSuccess ;
    float 			*pBuf = NULL ;
	int 			iSize = 0 ;
            
    for (iIndex = 0; iIndex < MAX_NUM_GPU; iIndex++)
    {
        cuError = cudaSetDevice (iIndex) ;
        if (cudaSuccess != cuError)
        {   
            printf ("cuMultiAlloc : cudaSetDevice Failed\n") ;
            return ;
        }
        if (iIndex == MAX_NUM_GPU-1)
		{
			iSize = (sizeof (float) * (pstCtx->iMultiSx1+2) *
                    (pstCtx->iMultiSx2) * (pstCtx->iMultiSx3)) ; 
		}
		else
		{
			iSize = (sizeof (float) * (pstCtx->iMultiSx1) *
                    (pstCtx->iMultiSx2) * (pstCtx->iMultiSx3)) ; 
		}
		pBuf = pfSrc + (iIndex * iSize) ;
        switch (eType)
        {
            case E_MULTI_COPY_TYPE_1:
            {
                if (E_MULTI_DEVICE_TO_DEVICE_FORWARD == eDirection)
                {
                    cuError = cudaMemcpy (pstCtx->fC1[iIndex], pBuf, iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
                else if (E_MULTI_DEVICE_TO_DEVICE_BACKWARD == eDirection)
                {
                    cuError = cudaMemcpy (pBuf, pstCtx->fC1[iIndex], iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
            }
            break ;
            case E_MULTI_COPY_TYPE_2:
            {
                if (E_MULTI_DEVICE_TO_DEVICE_FORWARD == eDirection)
                {
                    cuError = cudaMemcpy (pstCtx->fC2[iIndex], pBuf, iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
                else if (E_MULTI_DEVICE_TO_DEVICE_BACKWARD == eDirection)
                {
                    cuError = cudaMemcpy (pBuf, pstCtx->fC2[iIndex], iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
            }
            break ;
            case E_MULTI_COPY_TYPE_3:
            {
                if (E_MULTI_DEVICE_TO_DEVICE_FORWARD == eDirection)
                {
                    cuError = cudaMemcpy (pstCtx->fC3[iIndex], pBuf, iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
                else if (E_MULTI_DEVICE_TO_DEVICE_BACKWARD == eDirection)
                {
                    cuError = cudaMemcpy (pBuf, pstCtx->fC3[iIndex], iSize,
                                          cudaMemcpyDeviceToDevice) ;
                }
            }
            break ;

            default:
                printf ("Invalid type") ;    
        } 
        if (cudaSuccess != cuError)
        {
			printf ("cuMultiCopy Error : %s\n", cudaGetErrorString(cuError)) ;
            cuMultiHandleError(pstCtx) ;
        } 
    } 
}

void 
cuMultiCopyData (ST_MULTI_GPU_CTX *pstCtx, float *fData1, float *fData2, 
                 float *fData3, E_MULTI_DIRECTION eDir)
{
    cuMulitCopy (pstCtx, fData1, E_MULTI_COPY_TYPE_1, eDir) ;     
    cuMulitCopy (pstCtx, fData2, E_MULTI_COPY_TYPE_2, eDir) ;     
    cuMulitCopy (pstCtx, fData3, E_MULTI_COPY_TYPE_3, eDir) ;     
}

void 
cuMultiHandleError (ST_MULTI_GPU_CTX *pstCtx)
{
    int iIndex ;

#ifdef PRINT_ENTRY_EXIT
	printf ("Entered cuMultiHandleError\n") ;
#endif 

    for (iIndex = 0; iIndex < MAX_NUM_GPU; iIndex++)
    {
        cudaSetDevice (iIndex) ;
        cudaDeviceReset () ; 
    }
    if (NULL != pstCtx)
    {
        free (pstCtx) ;
        pstCtx = NULL ;
    }
#ifdef PRINT_ENTRY_EXIT
	printf ("Exited cuMultiHandleError\n") ;
#endif 
    exit(-1) ;
}




/*int cuIntializeCtx(ST_MULTI_GPU_CTX *pstCtx)
{
    int iRet = 0 ;
    pstCtx->pfC1 = (float *) calloc (1, sizeof(float)) ;
    if (NULL == pstCtx->pfC1)
    {
        printf ("cuIntializeCtx: Memory allocation error\n") ;
        iRet = -1 ;
    } 
    pstCtx->pfC2 = (float *) calloc (1, sizeof(float)) ;
    if (NULL == pstCtx->pfC2)
    {
        printf ("cuIntializeCtx: Memory allocation error\n") ;
        iRet = -1 ;
    } 
    pstCtx->pfC3 = (float *) calloc (1, sizeof(float)) ;
    if (NULL == pstCtx->pfC3)
    {
        printf ("cuIntializeCtx: Memory allocation error\n") ;
        iRet = -1 ;
    } 

    return iRet ;
}*/
#endif //USING_CUDA && MULTI_GPU
