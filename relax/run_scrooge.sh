#!/bin/bash
./waf

OUT=$?
echo $OUT 

export PATH=$PATH:/usr/local/cuda/bin:$PWD/build

if ! [ $OUT -eq 0 ];then
    ./waf configure --use-fftw --use-cuda --use-papi 
    ./waf
fi

./examples/tutorials/run2.sh 
#./examples/tutorials/run2.sh > temp.txt

#perl extTime.pl temp.txt
