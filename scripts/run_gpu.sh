#!/bin/bash
#SBATCH -p gpu
#SBATCH -n 1
#SBATCH -o result.log
#SBATCH --gres=gpu:1

export PATH=$PATH:~/workspace/relax_green/relax/build
relax --no-proj-output --no-grd-output run.inp
