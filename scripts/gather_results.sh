#!/bin/zsh

RELAX_EXPT_PATH=~/workspace/relax_green/scripts
RELAX_DATA_PATH=~/workspace/relax_green/data
pushd $RELAX_DATA_PATH


#cuda # fftw mkl cuda
for expt in cuda
do

echo "#dim, threads, expt, `$RELAX_EXPT_PATH/extTime.pl $RELAX_DATA_PATH/${expt}_t2_dim128128128/result.log 2`"

dims="512,512,256 256,256,256 128,128,128"
if [[ $expt == "mkl_st" || $expt == "cuda" ]]; then
	dims="512,512,256 \
        512,256,256 \
        256,256,256 \
        256,256,128 \
        256,128,128 \
        128,128,128 \
        64,64,64"
fi

for dim in `echo $dims`
do

if [[ $expt == "cuda" || $expt == "mkl_st" ]]; then
	total_threads=1
else 
	total_threads="1 2 4 8 16 32"
fi

for threads in `echo $total_threads`
do
	x=`echo $dim | cut -d"," -f 1`
	y=`echo $dim | cut -d"," -f 2`
	z=`echo $dim | cut -d"," -f 3`
	DATADIR=$RELAX_DATA_PATH/`echo ${expt} | sed "s/_st//g"`_t${threads}_dim$x$y$z
	pushd $DATADIR

	cp -uf $RELAX_EXPT_PATH/extTime.pl .
	cp -uf $RELAX_EXPT_PATH/parse_out.pl .
	cp -uf $RELAX_EXPT_PATH/relax_patterns.txt .
	cat result.log | grep -v "tensorfieldadd" | grep -v "bodyforces" > result.log1
	mv result.log1 result.log
	#echo "$x$y$z,$threads,$expt,`./extTime.pl result.log`"
	echo "$x$y$z,$threads,$expt,`./parse_out.pl . relax_patterns.txt`"

	popd
done
done
done

popd
