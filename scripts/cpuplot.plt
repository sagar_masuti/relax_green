#!/usr/bin/env gnuplot
# To plot the cpu timing 
# Author : Sagar Masuti.
# Date : 03-03-2015

set term postscript eps enhanced color
outputname="multi.pdf"
set output outputname

set size 0.6,0.5
set xlabel "function name"
set ylabel "time (sec)"

plot 'multi.txt' using 1:3:xtic(2) w l title "mkl1", "" using 1:4:xtic(2) w l title "mkl16"

system(sprintf("epstopdf %s",outputname));
