#!/bin/zsh

RELAX_EXPT_PATH=~/workspace/relax_green/scripts

./compile.sh 

RELAX_DATA_PATH=~/workspace/relax_green/data
mkdir -p $RELAX_DATA_PATH
pushd $RELAX_DATA_PATH

#for dim in 512,512,256 256,256,256 128,128,128
for dim in 128,128,128
do
for threads in 1 #32
do
	# 2 4 8 16
	x=`echo $dim | cut -d"," -f 1`
	y=`echo $dim | cut -d"," -f 2`
	z=`echo $dim | cut -d"," -f 3`
	DATADIR=$RELAX_DATA_PATH/mkl_t${threads}_dim$x$y$z
	mkdir -p $DATADIR
	pushd $DATADIR
	cp $RELAX_EXPT_PATH/run.sh .
        sed -i "s/OMP_NUM_THREADS=[0-9]*/OMP_NUM_THREADS=$threads/" run.sh 
        sed -i "s/SBATCH -n 0/SBATCH -n $threads/" run.sh 
#5i#SBATCH --gres=gpu:1

        sed -i "s/sx1 sx2 sx3/$x $y $z/" run.sh
	if [[ $1 -eq 1 ]]; then
		sbatch run.sh
	else 
		./run.sh
	fi
	popd
done
done

popd
