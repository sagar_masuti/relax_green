#!/usr/bin/perl

if ($#ARGV > 1) 
{
	print "usage: extractTime filename showheader\n";
	print "$#ARGV\n";
	exit;
}

my $ConStr = "L1 cache miss rate in" ;
my %timings ;
my %count ;
my $bPresent = 0 ;
my $temp = 0 ;

#open a file
open (MYFILE, $ARGV[0]);
while (<MYFILE>) {
	$temp = index ($_, 'L2') ;
	if ($temp != -1)
	{
		my $str = substr $_, length($ConStr), (length ($_) - length ($ConStr)) ;

		my @val = split(':', $str) ;	

		foreach my $k (keys %timings)
		{
			if ($k eq @val[0])
			{
				$bPresent = 1 ;
				$count{$k} = $count{$k} + 1 ;
				$timings{$k} = $timings{$k} + @val[1] ;
			}
  		}
		if ($bPresent == 0)
		{
			$timings{@val[0]} = @val[1] ;	
			$count{@val[0]} = 1 ;
		}
		$bPresent = 0 ;
	} 
 }
 my $avg ;
 my $sub ;
 my $total = keys %timings ;
 my $index = 0 ;

 
if( $ARGV[1] == 1 || $ARGV[1] == 2) {
 foreach my $k (sort keys %timings)
 {
        $k =~ s/^\s+|\s+$//g ;
     	if ($index == ($total-1))
	{
		print "$k" ;
	}
	else
	{
   		print "$k," ;
	}
	$index += 1 ;
 }
 print "\n" ;
}

if( $#ARGV == 0 || $ARGV[1] <= 1) {
 $index = 0 ;
 foreach my $k (sort keys %timings)
 {
		$k =~ s/^\s+|\s+$//g;
        print "$k," ;
        $index += 1 ;
 }
 print "\n" ;
}
if( $#ARGV == 0 || $ARGV[1] <= 1) {
 $index = 0 ;
 foreach my $k (sort keys %timings)
 {
        $avg = $timings{$k}/$count{$k} ;
        $sub = sprintf('%.5f', $avg);
    	$sub = 100 * $sub ;
	    print "$sub," ;
        $index += 1 ;
 }
 print "\n" ;

}

 close (MYFILE); 

